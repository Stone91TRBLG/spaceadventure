package com.trblg.game.control;

import com.badlogic.gdx.Input;

/**
 * Created by TRBLG on 14.02.2017.
 */

public class MenuControl implements Control {
    MenuControlHelper controlHelper;

    public MenuControl(MenuControlHelper controlHelper){
        this.controlHelper = controlHelper;
    }




    @Override
    public boolean keyDown(int keycode) {
        if((keycode == 131) || (keycode == Input.Keys.BACK)){
            //esc вызываем конструктор корабля
            controlHelper.esc();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        controlHelper.touchDownt(screenX, screenY);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        controlHelper.touchUp(screenX, screenY);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
