package com.trblg.game.control.buttons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by TRBLG on 15.02.2017.
 */

public class Button extends Actor {
    Sprite sprite;
    ClickHelper clickHelper;

    public Button(float x, float y, float width, float height){
        setBounds(x, y, width, height);

        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(clickHelper != null) {
                    clickHelper.clicked();
                }
                super.clicked(event, x, y);
            }
        });
    }

    public void setSprite(Sprite sprite){
        this.sprite = sprite;
        this.sprite.setBounds(getX(), getY(), getWidth(), getHeight());

    }

    public void setSprite(TextureRegion textureRegion){
        this.sprite = new Sprite(textureRegion);
        this.sprite.setBounds(getX(), getY(), getWidth(), getHeight());

    }

    public void setSprite(Texture texture){
        this.sprite = new Sprite(texture);
        this.sprite.setBounds(getX(), getY(), getWidth(), getHeight());

    }

    public void setClickHelper(ClickHelper clickHelper){
        this.clickHelper = clickHelper;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }
}
