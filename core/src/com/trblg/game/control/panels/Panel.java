package com.trblg.game.control.panels;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by TRBLG on 25.02.2017.
 */

public interface Panel {
    void update();
    void addToStage(Stage stage);
    void delete();
    void dispouse();
}
