package com.trblg.game.control.panels;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by TRBLG on 27.02.2017.
 */

public class ImgActor extends Actor {
    float x , y, size;
    Sprite sprite;


    public ImgActor(float x, float y, float size, Sprite sprite){
        this.x = x;
        this.y = y;
        this.size = size;
        this.sprite = sprite;
        setBounds(x, y, size, size);
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }

}
