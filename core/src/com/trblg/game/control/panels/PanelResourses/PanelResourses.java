package com.trblg.game.control.panels.PanelResourses;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.trblg.game.control.panels.ImgActor;
import com.trblg.game.control.panels.Panel;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.ResoursesOfPlayer;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 25.02.2017.
 */

public class PanelResourses implements Panel {
    private final float INDENT = 0.5f; // отступ от панели слева
    private final float sizeIcon = 1f;
    private Part boxes[][];
    private float panelWidth = 0;
    private float panelHigth = 3;

    private float x, y;

    private int power = 0;

    Sprite energyIconSprite, moneyIconSprite;

    ResoursesValueActor energy, money; // ресурсы энергия и ресурс для строительства(путь money почему нет)
    ImgActor energyIcon, moneyIcon;// значек ресурса слева от его значения

    public PanelResourses(Part boxes[][]){
        this.boxes = boxes;
        panelWidth = (Numbers.SIZE_OF_SIDE * Numbers.aspectRatio) -((Numbers.SIZE_OF_SIDE / 7) * 5);
        x = (Numbers.SIZE_OF_SIDE / 7) * 5 + INDENT;
        y = Numbers.SIZE_OF_SIDE - 0.3f;

        energyIconSprite = new Sprite(new Texture("icons/power.png"));
        moneyIconSprite  = new Sprite(new Texture("icons/resourse.png"));

        energyIcon = new ImgActor(x, y - sizeIcon, sizeIcon, energyIconSprite);
        moneyIcon = new ImgActor(x + panelWidth / 2, y - sizeIcon, sizeIcon, moneyIconSprite);

        System.out.println(panelWidth);

        energy = new ResoursesValueActor(x + sizeIcon + 0.1f, y);
        money = new ResoursesValueActor(x + sizeIcon + panelWidth / 2 + 0.1f, y);
    }

    @Override
    public void update() {
        sumPower();
        money.setResourse(ResoursesOfPlayer.getInstance().getResourses());
        energy.setResourse(power);
    }

    @Override
    public void addToStage(Stage stage) {
        stage.addActor(energyIcon);
        stage.addActor(moneyIcon);
        stage.addActor(energy);
        stage.addActor(money);
    }

    @Override
    public void delete() {

    }

    @Override
    public void dispouse() {

    }

    private void sumPower(){
        power = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null){
                    power -= boxes[i][j].getPower();
                }
            }
        }
    }

    public int getPower(){
        return power;
    }
}
