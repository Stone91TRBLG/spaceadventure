package com.trblg.game.control.panels.PanelResourses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by TRBLG on 27.02.2017.
 */

public class ResoursesValueActor extends Actor {
    BitmapFont font;
    int resourse = 0;
    float x , y;

    public ResoursesValueActor(float x, float y){
        this.x = x;
        this.y = y;

        font = new BitmapFont(Gdx.files.internal("fonts/font22.fnt"));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.setUseIntegerPositions(false);
        font.getData().setScale(0.05f);
        font.draw(batch, String.valueOf(resourse), x, y);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    public void setResourse(int resourse){
        this.resourse = resourse;
    }

}
