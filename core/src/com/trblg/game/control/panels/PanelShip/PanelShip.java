package com.trblg.game.control.panels.PanelShip;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.trblg.game.control.panels.*;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 25.02.2017.
 */

public class PanelShip implements Panel {

    Part boxes[][];
    BuildActor actors[][];
    ClickHelperBuilder clickHelper;
    Stage stage;


    public PanelShip(Part boxes[][], ClickHelperBuilder clickHelper){

        this.boxes = boxes;
        this.clickHelper = clickHelper;

        parseBoxes();
        createActors();
    }


    @Override
    public void update() {
        delete();
        parseBoxes();
        createActors();
        addToStage(stage);
    }

    @Override
    public void addToStage(Stage stage) {
        this.stage = stage;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(actors[i][j] != null){
                    stage.addActor(actors[i][j]);
                }
            }
        }
    }

    @Override
    public void delete() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null) {
                    if (boxes[i][j].getName().equals(Strings.BUILD_PART)) {
                        boxes[i][j] = null;
                    }
                }
            }
        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(actors[i][j] != null){
                    actors[i][j].remove();
                }
                actors[i][j] = null;
            }
        }
    }

    @Override
    public void dispouse() {

    }

    /*public void setBoxesToUpdate(Part boxes[][]){
        this.boxes = boxes;
    }*/

    private void parseBoxes(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null) {
                    if ((boxes[i][j].getName().equals(Strings.MAIN_BOX)) || (boxes[i][j].getName().equals(Strings.EXTRA_BOX))) {
                        if ((i > 0) && (boxes[i - 1][j] == null)) {
                            boxes[i - 1][j] = new BuildPart();
                        }

                        if ((j > 0) && (boxes[i][j - 1] == null)) {
                            boxes[i][j - 1] = new BuildPart();
                        }

                        if ((i < 4) && (boxes[i + 1][j] == null)) {
                            boxes[i + 1][j] = new BuildPart();
                        }

                        if ((i < 6) && (boxes[i][j + 1] == null)) {
                            boxes[i][j + 1] = new BuildPart();
                        }
                    }
                }
            }
        }
    }

    /*public boolean canDelete(int posX, int posY){
        //проверяем, можем ли продать

        if(boxes[posX][posY].getName().equals(Strings.EXTRA_BOX)){
            //проверяем, зависит ли какие то детали от этой основы строительства
            if ((posX > 0) && (boxes[posX - 1][posY] != null)) {
                if(boxes[posX - 1][posY].getName().equals(Strings.EXTRA_BOX) || boxes[posX - 1][posY].getName().equals(Strings.MAIN_BOX)){

                }else{
                    if(!haveMain(posX - 1,posY)){
                        return false;
                    }
                }
            }

            if ((posY > 0) && (boxes[posX][posY - 1] != null)) {
                if(boxes[posX][posY - 1].getName().equals(Strings.EXTRA_BOX) || boxes[posX][posY -1].getName().equals(Strings.MAIN_BOX)){

                }else{
                    if(!haveMain(posX, posY - 1)){
                        return false;
                    }
                }
            }

            if ((posX < 4) && (boxes[posX + 1][posY] != null)) {
                if(boxes[posX + 1][posY].getName().equals(Strings.EXTRA_BOX) || boxes[posX + 1][posY].getName().equals(Strings.MAIN_BOX)){

                }else{
                    if(!haveMain(posX + 1,posY)){
                        return false;
                    }
                }
            }

            if ((posY < 6) && (boxes[posX][posY + 1] != null)) {
                if(boxes[posX][posY + 1].getName().equals(Strings.EXTRA_BOX) || boxes[posX][posY + 1].getName().equals(Strings.MAIN_BOX)){

                }else{
                    if(!haveMain(posX, posY + 1)){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean haveMain(int i, int j){
        int count = 0;
        if((i > 0 ) && (boxes[i - 1][j] != null) && ((boxes[i - 1][j].getName().equals(Strings.EXTRA_BOX)) || (boxes[i - 1][j].getName().equals(Strings.MAIN_BOX)))){
            count++;
        }
        if((i < 4 ) && (boxes[i + 1][j] != null) && ((boxes[i + 1][j].getName().equals(Strings.EXTRA_BOX)) || (boxes[i + 1][j].getName().equals(Strings.MAIN_BOX)))){
            count++;
        }
        if((j > 0 ) && (boxes[i][j - 1] != null) && ((boxes[i][j - 1].getName().equals(Strings.EXTRA_BOX)) || (boxes[i][j - 1].getName().equals(Strings.MAIN_BOX)))){
            count++;
        }

        if((j < 6 ) && (boxes[i][j + 1] != null) && ((boxes[i][j + 1].getName().equals(Strings.EXTRA_BOX)) || (boxes[i][j + 1].getName().equals(Strings.MAIN_BOX)))){
            count++;
        }
        //проверяем, есть ли у данного элемента опора
        //должна иметь две опоры, т.к. одну мы собираемся удалить
        if(count > 1){
            return true;
        }
        return false;
    }*/

    private void createActors(){
        actors = new BuildActor[5][7];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null){
                    actors[i][j] = new BuildActor(i, j, clickHelper, boxes[i][j]);
                }
            }
        }
    }
}
