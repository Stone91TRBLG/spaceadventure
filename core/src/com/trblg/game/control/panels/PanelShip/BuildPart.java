package com.trblg.game.control.panels.PanelShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class BuildPart extends Part {
    public BuildPart(){
        setName(Strings.BUILD_PART);
        setSprite(new Sprite(new Texture("constructor/build part.png")));
    }
}
