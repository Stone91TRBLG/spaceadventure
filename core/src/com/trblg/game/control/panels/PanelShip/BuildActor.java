package com.trblg.game.control.panels.PanelShip;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class BuildActor extends Actor {
    int posX, posY;
    Sprite sprite;
    ClickHelperBuilder clickHelper;
    float sizeBox;

    public BuildActor(int x, int y, final ClickHelperBuilder clickHelper, Part part){
        posX = x;
        posY = y;
        sizeBox = Numbers.SIZE_OF_SIDE/7;
        this.clickHelper = clickHelper;
        sprite = part.getSprite();

        setBounds(x * sizeBox, y * sizeBox, sizeBox, sizeBox);
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());

        addListener(new ClickListener(){
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            clickHelper.clicked(posX, posY);
                            super.clicked(event, x, y);
                        }
                    }

        );


    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }
}
