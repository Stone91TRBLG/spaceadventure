package com.trblg.game.control.panels.PanelShip;

/**
 * Created by TRBLG on 26.02.2017.
 */

public interface ClickHelperBuilder {
    void clicked(int x, int y);
}
