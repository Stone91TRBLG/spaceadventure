package com.trblg.game.control.panels;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class SelectedActor extends Actor {
    private static SelectedActor instance;
    static int posX, posY;
    static Sprite sprite;
    float sizeBox;
    ClickHelperSelected clickHelperSelected;

    private SelectedActor(int x, int y, ClickHelperSelected clickHelperSelected){
        posX = x;
        posY = y;
        this.clickHelperSelected = clickHelperSelected;
        sizeBox = Numbers.SIZE_OF_SIDE/7;

        sprite = new Sprite(new Texture("constructor/select.png"));

        setBounds(x * sizeBox, y * sizeBox, sizeBox, sizeBox);
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());

        addListener(new ClickListener(){
                        @Override
                        public void clicked(InputEvent event, float x, float y) {

                            super.clicked(event, x, y);
                            //delete();
                            click();
                        }
                    }

        );


    }

    public static SelectedActor getInstance(int x, int y, ClickHelperSelected clickHelperSelected){
        if(instance == null){
            instance = new SelectedActor(x, y, clickHelperSelected);
        }
        instance.setPositionXY(x, y);
        return instance;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }

    public void delete(){
        this.remove();
    }

    private void setPositionXY(int x, int y){
        instance.setPosition(x * sizeBox, y * sizeBox);
    }

    private void click(){
        clickHelperSelected.clicked();
    }
}
