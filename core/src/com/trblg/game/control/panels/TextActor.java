package com.trblg.game.control.panels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by TRBLG on 27.02.2017.
 */

public class TextActor extends Actor {
    BitmapFont font;
    float x , y;
    String text;

    Skin skin;
    Label label;

    public TextActor(float x, float y, String text){
        this.x = x;
        this.y = y;
        this.text = text;

        font = new BitmapFont(Gdx.files.internal("fonts/font22.fnt"));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.setUseIntegerPositions(false);
        font.getData().setScale(0.05f);
        font.draw(batch, text, x, y);
    }
}
