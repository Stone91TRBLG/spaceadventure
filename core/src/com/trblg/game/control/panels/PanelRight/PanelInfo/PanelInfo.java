package com.trblg.game.control.panels.PanelRight.PanelInfo;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.trblg.game.control.buttons.Button;
import com.trblg.game.control.buttons.ClickHelper;
import com.trblg.game.control.panels.Panel;
import com.trblg.game.control.panels.ImgActor;
import com.trblg.game.control.panels.TextActor;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 25.02.2017.
 */

public class PanelInfo implements Panel {
    private final float INDENT = 0.5f;
    private final float IMG_SIZE = 3;
    float x, y;
    String name;
    String description;
    int price;
    Sprite sprite;

    private float panelWidth = 0;
    private float panelHight;

    ImgActor imgActor;
    TextActor nameActor;
    TextActor descriptionActor;
    ImgActor moneyIcon, energyIcon;
    TextActor priceActor, energyActor;
    Button button;
    ClickHelper clickHelper;


    public PanelInfo(Part part, ClickHelper clickHelper){
        this.clickHelper = clickHelper;
        x = (Numbers.SIZE_OF_SIDE / 7) * 5 + INDENT;
        y = 3.5f;

        panelWidth = (Numbers.SIZE_OF_SIDE * Numbers.aspectRatio) -((Numbers.SIZE_OF_SIDE / 7) * 5);
        panelHight = Numbers.SIZE_OF_SIDE - 3 - 1.5f;

        name = part.getName();
        description = part.getDescription();
        price = part.getPrice();
        sprite = new Sprite(part.getSprite());

        imgActor = new ImgActor(x, Numbers.SIZE_OF_SIDE - 3.5f - IMG_SIZE/2, IMG_SIZE, sprite);
        nameActor = new TextActor(x + IMG_SIZE + INDENT ,Numbers.SIZE_OF_SIDE - 3.5f + IMG_SIZE/2, name);
        descriptionActor = new TextActor(x, Numbers.SIZE_OF_SIDE - 3.5f - IMG_SIZE/2 - INDENT, description );

        if(name.equals(Strings.MAIN_BOX)) {
            } else{

            moneyIcon = new ImgActor(x + panelWidth / 50, y + INDENT, 1, new Sprite(new Texture("icons/resourse.png")));
            priceActor = new TextActor(x + panelWidth / 50 + 1.5f, y + INDENT + 1, String.valueOf(price/2));
            energyIcon = new ImgActor(x + panelWidth / 50 + 5f, y + INDENT, 1, new Sprite(new Texture("icons/power.png")));
            energyActor = new TextActor(x + panelWidth / 50 + 6.5f, y + INDENT + 1, String.valueOf(part.getPower()));
            button = new Button(x + panelWidth / 50 + 10f, y + INDENT, 3, 1);
            button.setSprite(new Texture("buttons/destroy_button.png"));
            button.setClickHelper(clickHelper);
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void addToStage(Stage stage) {
        stage.addActor(imgActor);
        stage.addActor(nameActor);
        stage.addActor(descriptionActor);

        if(name.equals(Strings.MAIN_BOX)) {
        } else{
            stage.addActor(moneyIcon);
            stage.addActor(priceActor);
            stage.addActor(energyIcon);
            stage.addActor(energyActor);
            stage.addActor(button);
        }
    }

    @Override
    public void delete() {
        imgActor.remove();
        nameActor.remove();
        descriptionActor.remove();
        if(name.equals(Strings.MAIN_BOX)) {
        } else{
            moneyIcon.remove();
            priceActor.remove();
            energyIcon.remove();
            energyActor.remove();
            button.remove();
        }
    }

    @Override
    public void dispouse() {

    }
}
