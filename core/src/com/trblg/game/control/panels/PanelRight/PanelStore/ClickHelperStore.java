package com.trblg.game.control.panels.PanelRight.PanelStore;


import com.trblg.game.model.PartsOfShip.Part;

/**
 * Created by TRBLG on 03.03.2017.
 */

public interface ClickHelperStore {
    void clicked(Part part, float x, float y);
}
