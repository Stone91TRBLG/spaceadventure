package com.trblg.game.control.panels.PanelRight.PanelStore;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.trblg.game.model.PartsOfShip.Part;
/**
 * Created by TRBLG on 03.03.2017.
 */

public class StoreActor extends Actor {
    float posX, posY;
    Sprite sprite;
    ClickHelperStore clickHelper;
    float sizeBox;
    Part part;

    public StoreActor(float x, float y, float sizeBox, final ClickHelperStore clickHelper, final Part part){
        this.part = part;
        posX = x;
        posY = y;
        this.sizeBox = sizeBox;

        this.clickHelper = clickHelper;
        sprite = new Sprite(part.getSprite());

        setBounds(x, y, sizeBox, sizeBox);
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());

        addListener(new ClickListener(){
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            clickHelper.clicked(part, posX, posY);
                            super.clicked(event, x, y);
                        }
                    }
        );
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }
}
