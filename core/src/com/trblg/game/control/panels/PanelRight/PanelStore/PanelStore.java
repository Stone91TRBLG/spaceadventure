package com.trblg.game.control.panels.PanelRight.PanelStore;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.trblg.game.control.buttons.Button;
import com.trblg.game.control.buttons.ClickHelper;
import com.trblg.game.control.panels.ClickHelperSelected;
import com.trblg.game.control.panels.ImgActor;
import com.trblg.game.control.panels.Panel;
import com.trblg.game.control.panels.TextActor;
import com.trblg.game.model.PartsOfShip.EnergyPart;
import com.trblg.game.model.PartsOfShip.EnginePart;
import com.trblg.game.model.PartsOfShip.ExtraPart;
import com.trblg.game.model.PartsOfShip.ImpulseGunPart;
import com.trblg.game.model.PartsOfShip.LaserGunPart;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.PartsOfShip.TorpedoGunPart;
import com.trblg.game.utils.Numbers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TRBLG on 25.02.2017.
 */

public class PanelStore implements Panel {
    private final float INDENT = 0.5f;
    List<Part> allParts = new ArrayList<Part>();
    List<StoreActor> storeActors = new ArrayList<StoreActor>();

    ClickHelper clickHelper;
    ClickHelperStore click, clickStore;
    StoreSelectedActor storeSelectedActor;


    private float panelWidth = 0;
    private float panelHight;
    private float startX, startY;
    private float sizeBox;
    private float buttonWidth = 4, buttonHight = 2;

    Stage stage;

    ImgActor moneyIcon, energyIcon;
    TextActor priceActor, energyActor;

    Button button;

    public PanelStore(ClickHelper clickHelper, ClickHelperStore click ){
        this.clickHelper = clickHelper;
        this.click = click;

        panelWidth = (Numbers.SIZE_OF_SIDE * Numbers.aspectRatio) -((Numbers.SIZE_OF_SIDE / 7) * 5);
        panelHight = Numbers.SIZE_OF_SIDE - 3 - 1.5f;
        startX = (Numbers.SIZE_OF_SIDE / 7) * 5 + INDENT;
        startY = 3.5f;
        sizeBox = Numbers.SIZE_OF_SIDE/8;


        button = new Button( startX + panelWidth - buttonWidth - INDENT * 2, startY + INDENT, 4, 2);
        button.setSprite(new Texture("buttons/buy_button.png"));
        button.setClickHelper(clickHelper);

        clickStore = new ClickHelperStore() {
            @Override
            public void clicked(Part part, float x, float y) {
                //промежуточный интерфейс. Прокидываю его дальше но тут,
                // можно отрисовывать параметры выбранного парта и выделить его иконку рамкой

                clickedDelete();
                System.out.println(part.getSprite().getX());
                storeSelectedActor = StoreSelectedActor.getInstance(x, y, sizeBox);

                moneyIcon = new ImgActor(startX + panelWidth / 50, startY + INDENT, 1, new Sprite(new Texture("icons/resourse.png")));
                priceActor = new TextActor(startX + panelWidth / 50 + 1.5f, startY + INDENT + 1, String.valueOf(-part.getPrice()));
                energyIcon = new ImgActor(startX + panelWidth / 50 + 5f, startY + INDENT, 1, new Sprite(new Texture("icons/power.png")));
                energyActor = new TextActor(startX + panelWidth / 50 + 6.5f, startY + INDENT + 1, String.valueOf(-part.getPower() ));

                clickedAddToStage();
                clickHelp(part, x, y);
            }
        };

        createPartsFoPanel();
        createActors();
    }


    @Override
    public void update() {

    }

    @Override
    public void addToStage(Stage stage) {
        this.stage = stage;
        for (int i = 0; i < storeActors.size(); i++) {
            stage.addActor(storeActors.get(i));
        }
        stage.addActor(button);
    }

    @Override
    public void delete() {
        for (int i = 0; i < storeActors.size(); i++) {
            storeActors.get(i).remove();
        }
        clickedDelete();
        button.remove();
    }

    @Override
    public void dispouse() {

    }

    private void createPartsFoPanel(){
        allParts.add(new ExtraPart());
        allParts.add(new EnginePart());
        allParts.add(new EnergyPart());
        allParts.add(new TorpedoGunPart());
        allParts.add(new ImpulseGunPart());
        allParts.add(new LaserGunPart());
    }

    private void createActors(){
        float x;
        float y = panelHight;
        int a = 0, i = 0, j = 0;

        while(a < allParts.size()){
            x = i * sizeBox + startX + INDENT * i ;
            if(x > Numbers.SIZE_OF_SIDE * Numbers.aspectRatio - sizeBox){
                i = 0;
                j++;
                y = panelHight - (sizeBox + INDENT) * j;
            }
            x = i * sizeBox + startX + INDENT * i ;
            storeActors.add(new StoreActor(x , y, sizeBox, clickStore, allParts.get(a)));

            a++;
            i++;
        }

    }

    private void clickHelp(Part part, float x, float y){
        click.clicked(part, x, y);
    }

    private void clickedAddToStage(){
        if(moneyIcon != null){
            stage.addActor(moneyIcon);
        }
        if(priceActor != null){
            stage.addActor(priceActor);
        }
        if(energyIcon != null){
            stage.addActor(energyIcon);
        }
        if(energyActor != null){
            stage.addActor(energyActor);
        }
        if(storeSelectedActor != null){
            stage.addActor(storeSelectedActor);
        }
    }

    private void clickedDelete(){
        if(moneyIcon != null){
            moneyIcon.remove();
        }
        if(priceActor != null){
            priceActor.remove();
        }
        if(energyIcon != null){
            energyIcon.remove();
        }
        if(energyActor != null){
            energyActor.remove();
        }
        if(storeSelectedActor != null){
            storeSelectedActor.remove();
        }
    }
}
