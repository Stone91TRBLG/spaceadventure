package com.trblg.game.control.panels.PanelRight.PanelStore;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by TRBLG on 06.03.2017.
 */

public class StoreSelectedActor extends Actor {
    private static StoreSelectedActor instance;
    static float posX, posY;
    static Sprite sprite;
    float sizeBox;


    private StoreSelectedActor(float x, float y, float sizeBox){
        this.sizeBox = sizeBox;
        posX = x;
        posY = y;
        sprite = new Sprite(new Texture("constructor/select.png"));

        setBounds(x, y, sizeBox, sizeBox);
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
    }

    public static StoreSelectedActor getInstance(float x, float y, float sizeBox){
        if(instance == null){
            instance = new StoreSelectedActor(x, y, sizeBox);
        }
        instance.setPositionXY(x, y);
        return instance;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }

    public void delete(){
        this.remove();
    }

    private void setPositionXY(float x, float y){
        instance.setPosition(x, y);
    }

}
