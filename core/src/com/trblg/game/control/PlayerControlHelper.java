package com.trblg.game.control;

/**
 * Created by TRBLG on 14.02.2017.
 */

public interface PlayerControlHelper {
    void down();
    void up();
    void esc();
    void space();
    void nothing();
}
