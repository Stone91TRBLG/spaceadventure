package com.trblg.game.control;

/**
 * Created by TRBLG on 14.02.2017.
 */

public interface MenuControlHelper {
    void esc();
    void touchDownt(int screenX, int screenY);
    void touchUp(int screenX, int screenY);
}
