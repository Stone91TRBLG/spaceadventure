package com.trblg.game.control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

/**
 * Created by TRBLG on 08.02.2017.
 */

public class PlayerControl implements Control {
    PlayerControlHelper controlHelper;



    public PlayerControl(PlayerControlHelper controlHelper){
        this.controlHelper = controlHelper;
    }

    @Override
    public boolean keyDown(int keycode) {
        System.out.println("keyDown: " + keycode);



        if((keycode == 131) || (keycode == Keys.BACK)){
            //esc вызываем конструктор корабля
            controlHelper.esc();
        }

        if((keycode == 51) || (keycode == Keys.UP)){
            controlHelper.up();
        }
        if((keycode == 47) || (keycode == Keys.DOWN)){
            controlHelper.down();
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
       // System.out.println("keyUp");
        controlHelper.nothing();
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
       // System.out.println("keyTyped: " + character);
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
       // System.out.println("touchDown");
        if(screenY < Gdx.graphics.getHeight()/2){
            controlHelper.up();
        }else{
            controlHelper.down();
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //System.out.println("touchUp: " + screenY);
        controlHelper.nothing();
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
       // System.out.println("touchDragged");
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        //System.out.println("mouseMoved");
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        //System.out.println("scrolled");
        return false;
    }




}
