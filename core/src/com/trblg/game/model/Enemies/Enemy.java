package com.trblg.game.model.Enemies;

import com.trblg.game.model.GameObject;

/**
 * Created by TRBLG on 11.02.2017.
 */

public abstract class Enemy extends GameObject {

    @Override
    public void destroyObject() {
        if(body == null) {
            return;
        }

        world.destroyBody(body);
        body = null;
    }
}
