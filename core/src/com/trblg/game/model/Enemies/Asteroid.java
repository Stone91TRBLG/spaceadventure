package com.trblg.game.model.Enemies;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.utils.GetRandom;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 09.02.2017.
 */

public class Asteroid extends Enemy {
    public static final String NAME = "asteroid";

    //private Body body;
    static FixtureDef fDef;
    private BodyDef bDef;
    private Fixture fixture;
    float radius;

    int hitpoints = 10;
    int damage = 1;

    public Asteroid(World world, float posX, float posY, float radius, int impulseX, int impulseY){

        this.radius = radius;
        createBody(world, posX, posY).applyForceToCenter(new Vector2(-impulseX * radius, impulseY), true);
        body.setUserData(this);
    }

    public Body createBody(World world, float posX, float posY){
        this.world = world;
        bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.DynamicBody;
        bDef.position.set(posX + Numbers.SIZE_OF_BOX, posY + Numbers.SIZE_OF_BOX);

        fDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        fDef.shape = shape;
        fDef.density = 10;

        body = world.createBody(bDef);
        fixture = body.createFixture(fDef);
        fixture.setUserData(NAME);
        body.setAngularVelocity(GetRandom.getFloat() * (GetRandom.getInt(0,3) - 1));// вращение
        return body;
    }

    @Override
    public void attack() {

    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public static Asteroid getRandomAsteroid(World world){
        return new Asteroid(world, Numbers.SIZE_OF_SIDE * 1.15f, GetRandom.getInt(1, 18),
                GetRandom.getInt(0, 2) + GetRandom.getFloat() + 0.3f,
                GetRandom.getInt(2000, 3000),
                GetRandom.getInt(0,100) - GetRandom.getInt(0,100));
    }


}
