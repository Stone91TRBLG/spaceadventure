package com.trblg.game.model.Enemies;

import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.utils.GetRandom;

/**
 * Created by TRBLG on 10.02.2017.
 */

public class EnemiesGenerator {
    World world;
    int flag = 300, count = 0;

    public EnemiesGenerator(World world){
        this.world = world;

    }

    public void addNewBody(){
        if(count >= flag) {
            count = 0;
            if(flag > 150) {
                flag--;
            }
            Asteroid.getRandomAsteroid(world);
        }
        count += GetRandom.getInt(1,3);
    }


}
