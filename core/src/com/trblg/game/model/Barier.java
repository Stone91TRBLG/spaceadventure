package com.trblg.game.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 13.02.2017.
 */

public class Barier extends GameObject {
    public static final String NAME = "barier";

    public Body body;
    static FixtureDef fDef;
    public BodyDef bDef;
    public Fixture fixture;

    public Barier(World world){
        this.world = world;
        createBody();
    }

    public void createBody(){
        bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.StaticBody;
        bDef.position.set(0, 1);


        if(fDef == null) {
            fDef = new FixtureDef();
            ChainShape shape = new ChainShape();
            shape.createChain(new Vector2[]{new Vector2(0 - Numbers.SIZE_OF_SIDE /2, 0 - Numbers.SIZE_OF_SIDE /2),
                    new Vector2(Numbers.SIZE_OF_SIDE * 1.5f, 0 - Numbers.SIZE_OF_SIDE /2),
                    new Vector2(Numbers.SIZE_OF_SIDE * 1.5f, Numbers.SIZE_OF_SIDE),
                    new Vector2(0 - Numbers.SIZE_OF_SIDE /2, Numbers.SIZE_OF_SIDE),
                    new Vector2(0 - Numbers.SIZE_OF_SIDE /2, 0 - Numbers.SIZE_OF_SIDE /2)});

            fDef.shape = shape;
        }

        body = world.createBody(bDef);
        fixture = body.createFixture(fDef);
        fixture.setUserData(NAME);

        body.setUserData(this);

    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void attack() {

    }

    @Override
    public float getHitpoints() {
        return 0;
    }

    @Override
    public void setHitpoints(int hitpoints) {

    }

    @Override
    public float getDamage() {
        return 0;
    }

    @Override
    public boolean hit(float damage) {
        return false;
    }

    @Override
    public void destroyObject() {

    }
}
