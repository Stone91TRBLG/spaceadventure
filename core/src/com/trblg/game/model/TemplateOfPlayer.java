package com.trblg.game.model;

import com.trblg.game.model.PartsOfShip.EnergyPart;
import com.trblg.game.model.PartsOfShip.ExtraPart;
import com.trblg.game.model.PartsOfShip.ImpulseGunPart;
import com.trblg.game.model.PartsOfShip.MainPart;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.PartsOfShip.TorpedoGunPart;

/**
 * Created by TRBLG on 18.02.2017.
 */

public class TemplateOfPlayer {
    private static Part boxes[][];
    ResoursesOfPlayer resourses;

    public TemplateOfPlayer(){
        resourses = ResoursesOfPlayer.getInstance();
        readSavedFile();
    }

    public void addBox(Part part, int i, int j){
        boxes[i][j] = part;
    }

    public void deleteBox(int i, int j){
        boxes[i][j] = null;
    }

    public Part[][] getBoxes(){
        return boxes;
    }

    private void readSavedFile(){
        if(boxes != null){
            return;
        }

        boxes = new Part[5][7];
        //читаем файл, если файл найден, то строим корабль по данным файла
        //если файла нет, то создаем стартовый корабль
        if(false){
            addBox(new MainPart(), 2, 0);
            addBox(new ExtraPart(), 2, 1);
            addBox(new ExtraPart(), 2, 2);
            addBox(new ExtraPart(), 1, 2);
            addBox(new ExtraPart(), 3, 2);

            addBox(new EnergyPart(), 1, 1);
            addBox(new EnergyPart(), 3, 1);

            addBox(new TorpedoGunPart(), 2, 3);
            addBox(new ImpulseGunPart(), 1, 3);
            addBox(new ImpulseGunPart(), 3, 3);

            resourses.setResourses(1000);

        }else{
            createStartShip();
            resourses.setResourses(1000);
        }
    }

    private void createStartShip(){
        //создаем стартовый корабль
        boxes[2][0] = new MainPart();
    }
}
