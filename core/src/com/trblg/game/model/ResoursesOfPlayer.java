package com.trblg.game.model;

/**
 * Created by TRBLG on 22.02.2017.
 */

public class ResoursesOfPlayer {
    private static ResoursesOfPlayer instance;
    int resourses;


    private ResoursesOfPlayer(){
        resourses = 0;
    }

    public static ResoursesOfPlayer getInstance(){
        if(instance == null){
            instance = new ResoursesOfPlayer();
        }
        return instance;
    }

    public int getResourses() {
        return resourses;
    }

    public void setResourses(int resourses) {
        this.resourses = resourses;
    }

    public void addResourses(int resourses){
        this.resourses += resourses;
    }

    public void removeResourses(int resourses){
        this.resourses -= resourses;
    }
}
