package com.trblg.game.model;

/**
 * Created by TRBLG on 09.02.2017.
 */

public interface Attack {
    //int hitpoint = 10;
    //int damage = 1;
    void attack();//метод, вызываемый для выполнения атаки
    float getHitpoints(); //получить здоровье
    void setHitpoints(int hitpoints);//установить здоровье
    float getDamage();//получить силу урона от объекта
    boolean hit(float damage);//метод вызывается, чтобы нанести объекту урон(уменьшить уровень хп.
                                    // возвращает тру, если запас здоровья <= 0
}
