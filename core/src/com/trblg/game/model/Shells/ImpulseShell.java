package com.trblg.game.model.Shells;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 11.02.2017.
 */

public class ImpulseShell extends Shell {
    public static final String NAME = "impulse shell";

    //private Body body;
    static FixtureDef fDef;
    private BodyDef bDef;
    private Fixture fixture;


    int hitpoints = 1;
    int damage = 1;


    public void createImpulse(World world, float posX, float posY, float speed) {
        this.world = world;
        bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.DynamicBody;
        bDef.position.set(posX + Numbers.SIZE_OF_BOX * 2, posY + (Numbers.SIZE_OF_BOX / Numbers.aspectRatio) / 2 - Numbers.SIZE_OF_IMPULSE);

        if(fDef == null) {
            fDef = new FixtureDef();
            CircleShape shape = new CircleShape();
            shape.setRadius(Numbers.SIZE_OF_IMPULSE);
            fDef.shape = shape;
            fDef.density = 0.5f;
        }

        body = world.createBody(bDef);
        fixture = body.createFixture(fDef);
        fixture.setUserData(NAME);
        body.setUserData(this);
       // body.applyForceToCenter(new Vector2(speed,0), true);
        body.setLinearVelocity(new Vector2(speed,0));

    }

    @Override
    public void attack() {

    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
