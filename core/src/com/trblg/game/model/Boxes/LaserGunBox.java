package com.trblg.game.model.Boxes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 07.02.2017.
 */
//Лазерное орудие, стреляющее постоянным фокусированным лучем энергии. Самое энергозатратное оружие.
//Прочность средняя.
//Достаточно дорогое
public class LaserGunBox extends Box {
    public String NAME = Strings.LASER_GUN_BOX;
    public static Texture texture = new Texture("boxes/LaserGunBox.png");

    public LaserGunBox(World world, Part part, int i, int j){
        super(world, i, j);
        sprite = new Sprite(texture);
    }

    @Override
    public void attack() {

    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public static Texture getTexture(){
        return texture;
    }
}
