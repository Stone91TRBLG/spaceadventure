package com.trblg.game.model.Boxes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.Shells.TorpedoShell;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 07.02.2017.
 */
//Орудие, выпускающее от одной до трех торпед за раз. Самое экономичное по затрату энергии.
//Имеет наибольшую прочность по сравнению с другими орудиями.
//Является самым дешевым видом оружия. Ранние версии торпедных установок, зряжаются торпедами низкого качества, которые могут не
//сдетонировать при контакте и отскочить в сторону
public class TorpedoGunBox extends Box {
    public String NAME = Strings.TORPEDO_GUN_BOX;
    public static Texture texture = new Texture("boxes/TorpedoGunBox.png");

    float cooldown = 50, cooldownCounter = 0; // скорость перезарядки
    float speed = 50;
    float hitpoints;
    float damage;

    public TorpedoGunBox(World world, Part part, int i, int j){
        super(world, i, j);
        sprite = new Sprite(texture);
    }


    public void attack() {
        if(cooldownCounter == cooldown) {
            new TorpedoShell().createTorpedo(world, getBody().getPosition().x, getBody().getPosition().y, speed);
            cooldownCounter = 0;
        }else{
            cooldownCounter++;
        }
    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public static Texture getTexture(){
        return texture;
    }
}
