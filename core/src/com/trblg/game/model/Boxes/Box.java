package com.trblg.game.model.Boxes;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.GameObject;
import com.trblg.game.model.Player;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 11.02.2017.
 */

public abstract class Box extends GameObject {
    public static final String NAME = "abstract box";
    int positionX, positionY;
    public static Sprite sprite;

    float hitpoints;
    float damage;


    public Box(World world, int i, int j){
        this.world = world;
        positionX = i;
        positionY = j;
        body = createBody(world, 0.5f + j * (Numbers.SIZE_OF_BOX * 2), 4 + i * (Numbers.SIZE_OF_BOX * 2));
        body.getFixtureList().get(0).setUserData(NAME);
        body.setUserData(this);
    }

    public void destroyObject(){

        if(body == null) {
            return;
        }

        world.destroyBody(body);
        Player.deleteBox(positionX, positionY);
        body = null;
    }



    public static Sprite getSprite(){
        return sprite;
    }




}
