package com.trblg.game.model.Boxes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 07.02.2017.
 */
//Являет собой основу корабля, от которой строятся остальные. При уничтожении этого блока, корабль считается уничтоженым.
//Имеет максимальный запас прочности
public class MainBox extends Box {
    public String NAME = Strings.MAIN_BOX;
    public static Texture texture = new Texture("boxes/MainBox.png");

    float hitpoints;
    float damage;

    public MainBox(World world, Part part , int i, int j){
        super(world, i, j);
        hitpoints = part.getHp();
        damage = Numbers.MAIN_BOX_DAMAGE;
        sprite = new Sprite(texture);
    }

    @Override
    public void attack() {

    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public static Texture getTexture(){
        return texture;
    }
}
