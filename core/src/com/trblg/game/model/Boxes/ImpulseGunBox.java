package com.trblg.game.model.Boxes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.Attack;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.Shells.ImpulseShell;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 07.02.2017.
 */
//Импульсное оружее. Стреляет пучками энергии. Имеет среднее энергопотребление. работает безотказно.
//Прочность на уровне среднего.
//Стоимасть средняя
public class ImpulseGunBox extends Box implements Attack {
    public String NAME = Strings.IMPULSE_GUN_BOX;
    public static Texture texture = new Texture("boxes/ImpulseGunBox.png");

    float cooldown = 30, cooldownCounter = 0; // скорость перезарядки
    float speed = 5;

    public ImpulseGunBox(World world, Part part, int i, int j){
        super(world, i, j);
        hitpoints = part.getHp();
        damage = Numbers.IMPULSE_GUN_BOX_DAMAGE;
        sprite = new Sprite(texture);
    }


    @Override
    public void attack() {
        if(cooldownCounter == cooldown) {
            new ImpulseShell().createImpulse(world, getBody().getPosition().x, getBody().getPosition().y, speed);
            cooldownCounter = 0;
        }else{
            cooldownCounter++;
        }
    }

    @Override
    public float getHitpoints() {
        return hitpoints;
    }

    @Override
    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public boolean hit(float damage) {
        hitpoints -= damage;

        if(hitpoints <= 0){
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public static Texture getTexture(){
        return texture;
    }
}
