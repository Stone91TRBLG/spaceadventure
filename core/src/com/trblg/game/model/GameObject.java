package com.trblg.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 07.02.2017.
 */

public abstract class GameObject implements Attack, destroyed{
    Sprite sprite;
    //int hitpoint, damage;
    public World world;
    public Body body;
    static FixtureDef fDef;
    public BodyDef bDef;
    public Fixture fixture;

    public void draw(){

    }

    public Body createBody(World world, float posX, float posY){
        this.world = world;
        bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.KinematicBody;
        bDef.position.set(posX + Numbers.SIZE_OF_BOX, posY + Numbers.SIZE_OF_BOX);

        if(fDef == null) {
            fDef = new FixtureDef();
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(Numbers.SIZE_OF_BOX, Numbers.SIZE_OF_BOX / (Gdx.graphics.getWidth() / Gdx.graphics.getHeight()));
            fDef.shape = shape;
        }

        body = world.createBody(bDef);
        fixture = body.createFixture(fDef);
        return body;
    }

    public Body getBody(){
        return body;
    }

    public Fixture getFixture(){
        return fixture;
    }



    public abstract String getName();

    /*public int getHitpoint(){
        return hitpoint;
    }

    public void setHitpoint(int hitpoint){
        this.hitpoint = hitpoint;
    }

    public int getDamage(){
        return damage;
    }*/


}
