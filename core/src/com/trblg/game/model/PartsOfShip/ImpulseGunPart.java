package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class ImpulseGunPart extends Part {

    public ImpulseGunPart(){
        setName(Strings.IMPULSE_GUN_BOX);
        setDescription(Strings.IMPULSE_GUN_BOX_DESCRIPTION);
        setHp(Numbers.IMPULSE_GUN_BOX_HP);
        setMaxHp(Numbers.IMPULSE_GUN_BOX_HP);
        setPower(Numbers.IMPULSE_GUN_BOX_ENERGY);
        setPrice(Numbers.IMPULSE_GUN_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/ImpulseGunBox.png")));
    }
}
