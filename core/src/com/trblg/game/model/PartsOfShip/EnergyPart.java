package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class EnergyPart extends Part {


    public EnergyPart(){
        setName(Strings.ENERGY_BOX);
        setDescription(Strings.ENERGY_BOX_DESCRIPTION);
        setHp(Numbers.ENERGY_BOX_HP);
        setMaxHp(Numbers.ENERGY_BOX_HP);
        setPower(Numbers.ENERGY_BOX_ENERGY);
        setPrice(Numbers.ENERGY_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/EnergyBox.png")));
    }
}
