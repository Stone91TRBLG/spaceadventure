package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class EnginePart  extends Part {

    public EnginePart(){
        setName(Strings.ENGINE_BOX);
        setDescription(Strings.ENGINE_BOX_DESCRIPTION);
        setHp(Numbers.ENGINE_BOX_HP);
        setMaxHp(Numbers.ENGINE_BOX_HP);
        setPower(Numbers.ENGINE_BOX_ENERGY);
        setPrice(Numbers.ENGINE_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/EngineBox.png")));
    }
}
