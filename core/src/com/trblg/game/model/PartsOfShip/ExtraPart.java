package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class ExtraPart extends Part {

    public ExtraPart(){
        setName(Strings.EXTRA_BOX);
        setDescription(Strings.EXTRA_BOX_DESCRIPTION);
        setHp(Numbers.EXTRA_BOX_HP);
        setMaxHp(Numbers.EXTRA_BOX_HP);
        setPower(Numbers.EXTRA_BOX_ENERGY);
        setPrice(Numbers.EXTRA_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/ExtraBox.png")));
    }
}
