package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by TRBLG on 26.02.2017.
 */

public abstract class Part {
    public String name; //название детали
    public int power; //количество потребляемой или выделяемой энергии
    public String description; // описание детали
    public float hp; //прочность детали
    public float maxHp; // максимальная прочность детали
    public Sprite sprite; // текстура детали
    public int price;// цена детали

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(float maxHp) {
        this.maxHp = maxHp;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
