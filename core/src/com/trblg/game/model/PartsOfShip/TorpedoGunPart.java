package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class TorpedoGunPart extends Part {

    public TorpedoGunPart(){
        setName(Strings.TORPEDO_GUN_BOX);
        setDescription(Strings.TORPEDO_GUN_BOX_DESCRIPTION);
        setHp(Numbers.TORPEDO_GUN_BOX_HP);
        setMaxHp(Numbers.TORPEDO_GUN_BOX_HP);
        setPower(Numbers.TORPEDO_GUN_BOX_ENERGY);
        setPrice(Numbers.TORPEDO_GUN_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/TorpedoGunBox.png")));
    }
}
