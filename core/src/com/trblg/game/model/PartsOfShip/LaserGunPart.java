package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
        import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class LaserGunPart extends Part {


    public LaserGunPart(){
        setName(Strings.LASER_GUN_BOX);
        setDescription(Strings.LASER_GUN_BOX_DESCRIPTION);
        setHp(Numbers.LASER_GUN_BOX_HP);
        setMaxHp(Numbers.LASER_GUN_BOX_HP);
        setPower(Numbers.LASER_GUN_BOX_ENERGY);
        setPrice(Numbers.LASER_GUN_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/LaserGunBox.png")));
    }
}
