package com.trblg.game.model.PartsOfShip;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class MainPart extends Part {
    public MainPart(){
        setName(Strings.MAIN_BOX);
        setDescription(Strings.MAIN_BOX_DESCRIPTION);
        setHp(Numbers.MAIN_BOX_HP);
        setMaxHp(Numbers.MAIN_BOX_HP);
        setPower(Numbers.MAIN_BOX_ENERGY);
        setPrice(Numbers.MAIN_BOX_PRICE);
        setSprite(new Sprite(new Texture("boxes/MainBox.png")));
    }

}
