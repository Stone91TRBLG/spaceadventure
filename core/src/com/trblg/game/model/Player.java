package com.trblg.game.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.Boxes.Box;
import com.trblg.game.model.Boxes.EnergyBox;
import com.trblg.game.model.Boxes.EngineBox;
import com.trblg.game.model.Boxes.ExtraBox;
import com.trblg.game.model.Boxes.ImpulseGunBox;
import com.trblg.game.model.Boxes.LaserGunBox;
import com.trblg.game.model.Boxes.MainBox;
import com.trblg.game.model.Boxes.TorpedoGunBox;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 07.02.2017.
 */

public class Player {

    private static Box[][] boxes;
    private Part[][] parts;
    private static TemplateOfPlayer templateOfPlayer;
    float speed = 5f;
    float movement = 0;
    World world;

    public Player(World world) {
        this.world = world;
        boxes = new Box[5][7];
        templateOfPlayer = new TemplateOfPlayer();
        parts = templateOfPlayer.getBoxes();
        createPlayer();
    }

    public void deleteAndDestroyBox(int i, int j){
        if(boxes[i][j] == null) {
            return;
        }
        boxes[i][j].destroyObject();
        boxes[i][j] = null;
    }

    public void playerAttack(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null){
                    boxes[i][j].attack();
                }
            }
        }
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void addBox(Box box, int i, int j){
        if(boxes[i][j] != null){
            boxes[i][j].destroyObject();
        }
        boxes[i][j] = box;

    }

    public static void deleteBox(int i, int j){
        boxes[i][j] = null;
        templateOfPlayer.deleteBox(i, j);
    }

    public void setVectorOfPlayer(float x, float y){
        movement = y;

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(boxes[i][j] != null) {
                    boxes[i][j].getBody().setLinearVelocity(new Vector2(x, y));
                }
            }
        }
    }

    public boolean stopUp(){
        if(boxes[2][0].getBody().getPosition().y > Numbers.SIZE_OF_SIDE / Numbers.aspectRatio - 0.5f){
            return true;
        }
        return false;
    }

    public boolean stopDown(){
        if(boxes[2][0].getBody().getPosition().y < 0.5f){
            return true;
        }
        return false;
    }

    public boolean limitCoordinat() {
        if ((movement < 0) && (stopDown())) {
            setVectorOfPlayer(0, 0);
            return true;
        }

        if ((movement > 0) && (stopUp())) {
            setVectorOfPlayer(0, 0);
            return true;
        }
        return false;

    }

    public static Box[][] getBoxes() {
        return boxes;
    }

    private void createPlayer(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                if(parts[i][j]!=null) {
                    if(parts[i][j].getName().equals(Strings.MAIN_BOX)){
                        addBox(new MainBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.EXTRA_BOX)){
                        addBox(new ExtraBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.ENERGY_BOX)){
                        addBox(new EnergyBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.ENGINE_BOX)){
                        addBox(new EngineBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.IMPULSE_GUN_BOX)){
                        addBox(new ImpulseGunBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.LASER_GUN_BOX)){
                        addBox(new LaserGunBox(world, parts[i][j], i, j), i, j);
                    }

                    if(parts[i][j].getName().equals(Strings.TORPEDO_GUN_BOX)){
                        addBox(new TorpedoGunBox(world, parts[i][j], i, j), i, j);
                    }
                }
            }
        }
    }
}
