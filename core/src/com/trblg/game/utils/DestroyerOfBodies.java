package com.trblg.game.utils;

import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.model.GameObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TRBLG on 12.02.2017.
 */

public class DestroyerOfBodies {
    List<GameObject> destroyList = new ArrayList<GameObject>();

    public List<GameObject> getDestroyList() {
        return destroyList;
    }

    //public void setBodyList(List<Body> destroyList) {
   //    this.destroyList = destroyList;
    //}

    public void addGameObjectToDestriyList(GameObject gameObject){
        destroyList.add(gameObject);
    }

    public boolean isEmpty(){
        if(destroyList.size()>0){
            return true;
        }
        return false;
    }

    public void desttroy(){
        for (int i = 0; i < destroyList.size(); i++) {
            destroyList.get(i).destroyObject();
        }
        destroyList.clear();
    }

}
