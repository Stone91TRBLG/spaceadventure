package com.trblg.game.utils;

/**
 * Created by TRBLG on 26.02.2017.
 */

public class Strings {
    //название деталей корабля
    public static final String MAIN_BOX = "main box";
    public static final String ENERGY_BOX = "energy box";
    public static final String ENGINE_BOX = "engine box";
    public static final String EXTRA_BOX = "extra box";
    public static final String IMPULSE_GUN_BOX = "impulse gun box";
    public static final String LASER_GUN_BOX = "laser gun box";
    public static final String TORPEDO_GUN_BOX = "torpedo gun box";

    public static final String BUILD_PART = "build part";

    public static final String MAIN_BOX_DESCRIPTION = "описание на русском й Ы";
    public static final String ENERGY_BOX_DESCRIPTION = "description";
    public static final String ENGINE_BOX_DESCRIPTION = "description";
    public static final String EXTRA_BOX_DESCRIPTION = "description";
    public static final String IMPULSE_GUN_BOX_DESCRIPTION = "description";
    public static final String LASER_GUN_BOX_DESCRIPTION = "description";
    public static final String TORPEDO_GUN_BOX_DESCRIPTION = "description";
}
