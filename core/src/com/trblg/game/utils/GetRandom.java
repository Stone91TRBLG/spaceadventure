package com.trblg.game.utils;


import java.util.Random;

/**
 * Created by TRBLG on 10.02.2017.
 */

public class GetRandom {
    public static Random random = new Random();

    public static int getInt(int startNumber, int diaposon){
        int r = random.nextInt(diaposon) + startNumber;
        return r;
    }

    public static float getFloat(){

        float r = random.nextFloat();
        return r;
    }
}
