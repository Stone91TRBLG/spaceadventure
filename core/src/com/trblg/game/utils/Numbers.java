package com.trblg.game.utils;

/**
 * Created by TRBLG on 07.02.2017.
 */

public class Numbers {
   public static final float MAIN_BOX_HP = 10;
   public static final float ENERGY_BOX_HP = 10;
   public static final float ENGINE_BOX_HP = 10;
   public static final float EXTRA_BOX_HP = 10;
   public static final float IMPULSE_GUN_BOX_HP = 10;
   public static final float LASER_GUN_BOX_HP = 10;
   public static final float TORPEDO_GUN_BOX_HP = 10;

   public static final float MAIN_BOX_DAMAGE = 1;
   public static final float ENERGY_BOX_DAMAGE = 1;
   public static final float ENGINE_BOX_DAMAGE = 1;
   public static final float EXTRA_BOX_DAMAGE = 1;
   public static final float IMPULSE_GUN_BOX_DAMAGE = 1;
   public static final float LASER_GUN_BOX_DAMAGE = 1;
   public static final float TORPEDO_GUN_BOX_DAMAGE = 1;

   public static final int MAIN_BOX_ENERGY = 0;
   public static final int ENERGY_BOX_ENERGY = -200;
   public static final int ENGINE_BOX_ENERGY = 30;
   public static final int EXTRA_BOX_ENERGY = 15;
   public static final int IMPULSE_GUN_BOX_ENERGY = 30;
   public static final int LASER_GUN_BOX_ENERGY = 40;
   public static final int TORPEDO_GUN_BOX_ENERGY = 10;

   public static final int MAIN_BOX_PRICE = 0;
   public static final int ENERGY_BOX_PRICE = 200;
   public static final int ENGINE_BOX_PRICE = 300;
   public static final int EXTRA_BOX_PRICE = 150;
   public static final int IMPULSE_GUN_BOX_PRICE = 300;
   public static final int LASER_GUN_BOX_PRICE = 400;
   public static final int TORPEDO_GUN_BOX_PRICE = 100;

   public static final float SIZE_OF_SIDE = 24;

   public static final float SIZE_OF_BOX = 0.3f;
   public static final float SIZE_OF_TORPEDO = 0.1f;
   public static final float SIZE_OF_IMPULSE = 0.09f;

   public static float aspectRatio;

}
