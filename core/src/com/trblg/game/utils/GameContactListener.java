package com.trblg.game.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.trblg.game.model.Barier;
import com.trblg.game.model.Enemies.Asteroid;
import com.trblg.game.model.GameObject;
import com.trblg.game.model.Shells.TorpedoShell;

/**
 * Created by TRBLG on 12.02.2017.
 */

public class GameContactListener implements ContactListener {
    DestroyerOfBodies destroyer;

    public GameContactListener(DestroyerOfBodies destroyer){
        this.destroyer = destroyer;
    }

    @Override
    public void beginContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        GameObject A, B;
        Body bodyA, bodyB;
        boolean boolA, boolB;
        String nameA, nameB;

        bodyA = contact.getFixtureA().getBody();
        bodyB = contact.getFixtureB().getBody();

        A = (GameObject)bodyA.getUserData();
        B = (GameObject)bodyB.getUserData();

        nameA = A.getName();
        nameB = B.getName();

        System.out.println("A: " + nameA + "  B: " + nameB);

        if((nameA.equals(nameB)) && (nameA.equals(Asteroid.NAME))){
            return;
        }

        if(nameA.equals(Barier.NAME)){
            destroyer.addGameObjectToDestriyList(B);
            return;
        }

        if(nameB.equals(Barier.NAME)){
            destroyer.addGameObjectToDestriyList(A);
            return;
        }

        if((nameA.equals(TorpedoShell.NAME)) || (nameB.equals(TorpedoShell.NAME)) ){
            //осечка. торпеда не сдетонировала

            if(GetRandom.getInt(0, 1000)  <  10) {
                return;
            }
        }



        boolA = A.hit(B.getDamage());
        boolB = B.hit(A.getDamage());
        if(boolA){
            destroyer.addGameObjectToDestriyList(A);
        }
        if(boolB){
            destroyer.addGameObjectToDestriyList(B);
        }


    }

}
