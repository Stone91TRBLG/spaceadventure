package com.trblg.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.trblg.game.view.GameScreen;
import com.trblg.game.view.StartScreen;

public class SpaceAdventure extends Game {
	Screen screen;
	
	@Override
	public void create () {
		screen = StartScreen.getInstance(this);
		setScreen(screen);
	}
}
