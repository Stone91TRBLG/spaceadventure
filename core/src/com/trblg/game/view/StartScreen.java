package com.trblg.game.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.trblg.game.control.buttons.Button;
import com.trblg.game.control.buttons.ClickHelper;


/**
 * Created by TRBLG on 15.02.2017.
 */

public class StartScreen implements Screen {
    public static StartScreen instance;
    Game game;

    OrthographicCamera camera;
    float aspectRatio, sizeOfSide = 24;

    Texture textureBackground, textureButtonSettings, textureButtonConstructor, textureButtonExit;
    Image background;

    Stage stage;
    Button buttonSettings, buttonConstructor, buttonExit;

    private StartScreen(Game game){
        this.game = game;
        //if(world == null) {
        //    this.vector = new Vector2(0, 0);
       //     this.world = new World(vector, true);
        //}
    }

    public static StartScreen getInstance(Game game){
        if(instance == null){
            return new StartScreen(game);
        }
        return instance;
    }



    @Override
    public void show() {
        aspectRatio = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        camera = new OrthographicCamera(sizeOfSide * aspectRatio, sizeOfSide);

        stage = new Stage(new FillViewport(sizeOfSide * aspectRatio, sizeOfSide, camera));
        Gdx.input.setInputProcessor(stage);
        initializationButtons();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        aspectRatio = (float) width / height;
        camera = new OrthographicCamera(sizeOfSide * aspectRatio, sizeOfSide);
        camera.viewportWidth = sizeOfSide * aspectRatio;
        camera.viewportHeight = sizeOfSide;
        camera.position.set(new Vector3(sizeOfSide * aspectRatio/2, sizeOfSide / 2, 0));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
       // world.dispose();
        stage.dispose();
        textureBackground.dispose();
        textureButtonSettings.dispose();
        textureButtonConstructor.dispose();
        textureButtonExit.dispose();
    }



    public void comeToGameScreen(){
        game.setScreen(new GameScreen(game));
    }

    public void comeToSettingsScreen(){
        game.setScreen(new SettingsScreen(game, this));
    }

    public void comeToConstuctorScreen(){
        game.setScreen(new PlayerConstructorScreen(game, this));
    }


    public void initializationButtons(){
        textureBackground = new Texture("menu/start screen.png");
        background = new Image(textureBackground);
        background.setBounds(0, 0, sizeOfSide * aspectRatio, sizeOfSide );
        stage.addActor(background);

        textureButtonSettings = new Texture("buttons/settings_button.png");
        textureButtonConstructor = new Texture("buttons/constructor_button.png");
        textureButtonExit = new Texture("buttons/exit_button.png");

        buttonConstructor = new Button(0, 0, 6, 3);
        buttonSettings = new Button(6, 0 , 6, 3);
        buttonExit = new Button(12, 0, 6, 3);

        buttonSettings.setSprite(textureButtonSettings);
        buttonConstructor.setSprite(textureButtonConstructor);
        buttonExit.setSprite(textureButtonExit);

        stage.addActor(buttonSettings);
        stage.addActor(buttonConstructor);
        stage.addActor(buttonExit);

        buttonSettings.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeToSettingsScreen();
            }
        });

        buttonConstructor.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeToConstuctorScreen();
            }
        });

        buttonExit.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                //exit();
                comeToGameScreen();
            }
        });

    }

    public void exit(){
        //сохранить прогресс
        dispose();
        System.exit(0);
    }


}
