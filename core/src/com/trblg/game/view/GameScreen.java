package com.trblg.game.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.trblg.game.control.PlayerControlHelper;
import com.trblg.game.control.PlayerControl;
import com.trblg.game.model.Barier;
import com.trblg.game.model.Enemies.EnemiesGenerator;
import com.trblg.game.model.Player;
import com.trblg.game.utils.DestroyerOfBodies;
import com.trblg.game.utils.GameContactListener;
import com.trblg.game.utils.Numbers;

/**
 * Created by TRBLG on 07.02.2017.
 */

public class GameScreen implements Screen {
    private Game game;
    OrthographicCamera camera;
    Box2DDebugRenderer render;
    World world;
    Vector2 vector;
    PlayerControlHelper controlHelper;
    PlayerControl playerControl;


    Player player;
    EnemiesGenerator generator;
    GameContactListener contactListener;
    DestroyerOfBodies destroyer;

    //Texture textureBackground;
    //SpriteBatch batch;

    public GameScreen(Game game){
        this.game = game;
        vector = new Vector2(0, 0);
        this.world = new World(vector, true);
        render = new Box2DDebugRenderer();


        //textureBackground = new Texture("badlogic.jpg");
        //batch = new SpriteBatch();


        player = new Player(world);
        generator = new EnemiesGenerator(world);
        destroyer = new DestroyerOfBodies();
        contactListener = new GameContactListener(destroyer);
        world.setContactListener(contactListener);
        new Barier(world);


    }

    @Override
    public void show() {

        controlHelper = new PlayerControlHelper() {
            @Override
            public void down() {
                if(player.stopDown()){
                    return;
                }
                player.setVectorOfPlayer(0, -player.getSpeed());
            }

            @Override
            public void up() {
                if(player.stopUp()){
                    return;
                }
                player.setVectorOfPlayer(0, player.getSpeed());
            }

            @Override
            public void esc() {

                comeToExitScreen();
            }

            @Override
            public void space() {

            }

            @Override
            public void nothing() {
                player.setVectorOfPlayer(0 ,0);
            }
        };

        playerControl = new PlayerControl(controlHelper);
        Gdx.input.setInputProcessor(playerControl);

        /*vector = new Vector2(0, 0);
        world = new World(vector, true);
        render = new Box2DDebugRenderer();


        //textureBackground = new Texture("badlogic.jpg");
        //batch = new SpriteBatch();


        player = Player.getInstance(world);
        generator = new EnemiesGenerator(world);
        destroyer = new DestroyerOfBodies();
        contactListener = new GameContactListener(destroyer);
        world.setContactListener(contactListener);
        new Barier(world);

        controlHelper = new PlayerControlHelper() {
            @Override
            public void down() {
                if(player.stopDown()){
                    return;
                }
                player.setVectorOfPlayer(0, -player.getSpeed());
            }

            @Override
            public void up() {
                if(player.stopUp()){
                    return;
                }
                player.setVectorOfPlayer(0, player.getSpeed());
            }

            @Override
            public void esc() {

                //setMenuScreen();
            }

            @Override
            public void esc() {

            }

            @Override
            public void nothing() {
                player.setVectorOfPlayer(0 ,0);
            }
        };

        playerControl = new PlayerControl(controlHelper);
        Gdx.input.setInputProcessor(playerControl);*/


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        render.render(world, camera.combined);

        generator.addNewBody();

        world.step(1/60f, 4, 4);

        destroyer.desttroy();

        camera.update();

        player.limitCoordinat();
        player.playerAttack();


        //batch.setProjectionMatrix(camera.combined);
        //batch.begin();
//
        //batch.draw(textureBackground, 0, 0, 2, 2);
        //batch.end();
    }

    @Override
    public void resize(int width, int height) {
        Numbers.aspectRatio = (float) width / height;
        camera = new OrthographicCamera(Numbers.SIZE_OF_SIDE, Numbers.SIZE_OF_SIDE / Numbers.aspectRatio);
        camera.viewportWidth = Numbers.SIZE_OF_SIDE;
        camera.viewportHeight = Numbers.SIZE_OF_SIDE / Numbers.aspectRatio;
        camera.position.set(new Vector3(Numbers.SIZE_OF_SIDE /2, (Numbers.SIZE_OF_SIDE / Numbers.aspectRatio) / 2, 0));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
    }

    public void comeToExitScreen(){
        game.setScreen(new ExitScreen(game, this));
    }

    public void comeToStartScreen(){
        game.setScreen(StartScreen.getInstance(game));
    }
}
