package com.trblg.game.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.trblg.game.control.buttons.Button;
import com.trblg.game.control.buttons.ClickHelper;
import com.trblg.game.control.panels.ClickHelperSelected;
import com.trblg.game.control.panels.Panel;
import com.trblg.game.control.panels.PanelResourses.PanelResourses;
import com.trblg.game.control.panels.PanelRight.PanelInfo.PanelInfo;
import com.trblg.game.control.panels.PanelRight.PanelStore.ClickHelperStore;
import com.trblg.game.control.panels.PanelRight.PanelStore.PanelStore;
import com.trblg.game.control.panels.PanelShip.ClickHelperBuilder;
import com.trblg.game.control.panels.PanelShip.PanelShip;
import com.trblg.game.control.panels.SelectedActor;
import com.trblg.game.model.PartsOfShip.Part;
import com.trblg.game.model.ResoursesOfPlayer;
import com.trblg.game.model.TemplateOfPlayer;
import com.trblg.game.utils.Numbers;
import com.trblg.game.utils.Strings;

/**
 * Created by TRBLG on 14.02.2017.
 */

public class PlayerConstructorScreen implements Screen {
    Game game;
    Screen screen;

    OrthographicCamera camera;

    Texture textureButtonBack, textureButtonAccept;
    Stage stage;

    Button buttonBack, buttonAccept;

    TemplateOfPlayer templateOfPlayer;
    Part boxes[][];

    int posX, posY;

    PanelShip panelShip;
    PanelResourses panelResourses;
    Panel panelRight;

    SelectedActor selectedActor;
    private Part selectedToBuyPart;


    ClickHelperBuilder clickHelperBuilder;
    ClickHelperSelected clickHelperSelected;
    ClickHelper clickHelperDestroy;
    ClickHelper clickHelperBuy;
    ClickHelperStore clickHelperStore;

    public PlayerConstructorScreen(Game game, Screen screen){
        this.game = game;
        this.screen = screen;
        templateOfPlayer = new TemplateOfPlayer();
        this.boxes = templateOfPlayer.getBoxes();

        initializationClickHelpers();
    }

    @Override
    public void show() {
        Numbers.aspectRatio = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();

        camera = new OrthographicCamera(Numbers.SIZE_OF_SIDE * Numbers.aspectRatio , Numbers.SIZE_OF_SIDE);

        stage = new Stage(new FillViewport(Numbers.SIZE_OF_SIDE * Numbers.aspectRatio, Numbers.SIZE_OF_SIDE, camera));
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        stage.act(delta);
        stage.draw();

        panelResourses.update();
    }

    @Override
    public void resize(int width, int height) {
        Numbers.aspectRatio = (float) width / height;
        camera = new OrthographicCamera(Numbers.SIZE_OF_SIDE * Numbers.aspectRatio, Numbers.SIZE_OF_SIDE);
        camera.viewportWidth = Numbers.SIZE_OF_SIDE * Numbers.aspectRatio;
        camera.viewportHeight = Numbers.SIZE_OF_SIDE;
        camera.position.set(new Vector3((Numbers.SIZE_OF_SIDE * Numbers.aspectRatio)/2, (Numbers.SIZE_OF_SIDE) / 2, 0));

        panelShip = new PanelShip(boxes, clickHelperBuilder);
        panelResourses = new PanelResourses(boxes);


        initializationButtons();
        panelShip.addToStage(stage);
        panelResourses.addToStage(stage);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void comeBackToStartScreen(){
        game.setScreen(screen);
    }

    private void initializationButtons(){

        textureButtonBack = new Texture("buttons/back_button.png");
        textureButtonAccept = new Texture("buttons/menu_button.png");

        float spaceBetweenButtons = 0.5f;
        float widthOfButtons = ( (Numbers.SIZE_OF_SIDE * Numbers.aspectRatio -(((Numbers.SIZE_OF_SIDE) /7) * 5)) - spaceBetweenButtons*3) / 2;// вычесляем пространство от левой понели до края. а потом делим на колличество кнопок

        buttonBack = new Button((Numbers.SIZE_OF_SIDE / 7)*5 + spaceBetweenButtons, 0 , widthOfButtons, 3);
        buttonAccept = new Button((Numbers.SIZE_OF_SIDE / 7)*5 + spaceBetweenButtons + widthOfButtons + spaceBetweenButtons , 0, widthOfButtons, 3);

        buttonAccept.setSprite(textureButtonAccept);
        buttonBack.setSprite(textureButtonBack);


        stage.addActor(buttonAccept);
        stage.addActor(buttonBack);


        buttonAccept.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeToStartScreen();
            }
        });

        buttonBack.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeBackToStartScreen();
            }
        });


    }

    private void initializationClickHelpers(){
        clickHelperBuilder = new ClickHelperBuilder() {
            @Override
            public void clicked(int x, int y) {
                panelShipClicked(x, y);

            }
        };

        clickHelperSelected = new ClickHelperSelected() {
            @Override
            public void clicked() {
                if(selectedActor != null) {
                    selectedActor.delete();
                    selectedActor = null;
                }
                if(panelRight != null) {
                    panelRight.delete();
                    panelRight = null;
                }
            }
        };

        clickHelperDestroy = new ClickHelper() {
            @Override
            public void clicked() {
                //проверка на воможность удаления
                if(true/*panelShip.canDelete(posX, posY)*/) {

                    if (selectedActor != null) {
                        selectedActor.delete();
                        selectedActor = null;
                    }
                    if (panelRight != null) {
                        panelRight.delete();
                        panelRight = null;
                    }

                    ResoursesOfPlayer.getInstance().addResourses(boxes[posX][posY].getPrice() / 2);
                    boxes[posX][posY] = null;

                    panelResourses.update();
                    panelShip.update();
                }
            }
        };

        clickHelperBuy = new ClickHelper() {
            @Override
            public void clicked() {
                //проверяем, может ли игрок купить... Если да то вычетаем ресурсы и устанавливаем деталь
                int res = ResoursesOfPlayer.getInstance().getResourses();
                if(res - selectedToBuyPart.getPrice() >= 0){
                    if(panelResourses.getPower() - selectedToBuyPart.getPower() >= 0){

                        ResoursesOfPlayer.getInstance().setResourses(res - selectedToBuyPart.getPrice());
                        boxes[posX][posY] = selectedToBuyPart;

                        if (selectedActor != null) {
                            selectedActor.delete();
                            selectedActor = null;
                        }

                        if (panelRight != null) {
                            panelRight.delete();
                            panelRight = null;
                        }

                        panelResourses.update();
                        panelShip.update();
                    }
                }
            }
        };

        clickHelperStore = new ClickHelperStore() {
            @Override
            public void clicked(Part part, float x, float y) {
                selectedToBuyPart = part;
            }
        };
    }

    public void comeToStartScreen(){
        game.setScreen(StartScreen.getInstance(game));
    }

    private void panelShipClicked(int x, int y){

        posX = x;
        posY = y;

        selectedActor = SelectedActor.getInstance(x, y, clickHelperSelected);
        stage.addActor(selectedActor);

        if(panelRight != null){
            panelRight.delete();
            panelRight = null;
        }

        if(boxes[x][y].getName().equals(Strings.BUILD_PART)){
            //создаем панель строительства
            panelRight = new PanelStore(clickHelperBuy, clickHelperStore);
            panelRight.addToStage(stage);
        }else {
            panelRight = new PanelInfo(boxes[x][y], clickHelperDestroy);
            panelRight.addToStage(stage);
        }
    }
}