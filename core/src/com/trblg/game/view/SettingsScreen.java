package com.trblg.game.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.trblg.game.control.buttons.ClickHelper;
import com.trblg.game.control.buttons.Button;

/**
 * Created by TRBLG on 15.02.2017.
 */

public class SettingsScreen implements Screen {
    Game game;
    Screen exitScreen;

    OrthographicCamera camera;
    float aspectRatio, sizeOfSide = 24;

    Texture textureBackground, textureButtonBack;
    Image background;


    Stage stage;
    Button buttonBack;


    public SettingsScreen(Game game, Screen screen){
        this.game = game;
        this.exitScreen = screen;
    }

    @Override
    public void show() {
        textureBackground = new Texture("menu/settings screen.png");
        textureButtonBack = new Texture("buttons/back_button.png");
        background = new Image(textureBackground);

        aspectRatio = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        camera = new OrthographicCamera(sizeOfSide, sizeOfSide / aspectRatio);

        stage = new Stage(new FillViewport(sizeOfSide, sizeOfSide/aspectRatio, camera));

        buttonBack = new Button(sizeOfSide/2 - 4, 3 , 8, 3);
        buttonBack.setSprite(textureButtonBack);


        stage.addActor(background);
        stage.addActor(buttonBack);

        buttonBack.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeBack();
            }
        });

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        aspectRatio = (float) width / height;
        camera = new OrthographicCamera(sizeOfSide, sizeOfSide / aspectRatio);
        camera.viewportWidth = sizeOfSide;
        camera.viewportHeight = sizeOfSide / aspectRatio;
        camera.position.set(new Vector3(sizeOfSide/2, (sizeOfSide / aspectRatio) / 2, 0));

        background.setBounds(0, 0, sizeOfSide, sizeOfSide / aspectRatio );
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


    public void comeBack(){
        game.setScreen(exitScreen);
    }
}
