package com.trblg.game.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.trblg.game.control.buttons.ClickHelper;
import com.trblg.game.control.buttons.Button;

/**
 * Created by TRBLG on 14.02.2017.
 */

public class ExitScreen implements Screen {
    //скриин вызывается во время игры и ставит ее на паузу. в нем можно выйти  на StartScreen, на SettingsScreen
    // или вернуться и продолжить игру

    Screen gameScreen;
    Game game;

    OrthographicCamera camera;
    float aspectRatio, sizeOfSide = 24;

    Texture textureBackground, textureButtonBack, textureButtonSettings, textureButtonExitToMenu;
    Image background;


    Stage stage;
    Button buttonBack, buttonSettings, buttonExitToMenu;


    public ExitScreen(Game game, Screen gameScreen){
        this.game = game;
        this.gameScreen = gameScreen;
    }


    @Override
    public void show() {
        textureBackground = new Texture("menu/exit screen.png");
        background = new Image(textureBackground);

        aspectRatio = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        camera = new OrthographicCamera(sizeOfSide, sizeOfSide / aspectRatio);

        stage = new Stage(new FillViewport(sizeOfSide, sizeOfSide/aspectRatio, camera));

        initializationButtons();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        aspectRatio = (float) width / height;
        camera = new OrthographicCamera(sizeOfSide, sizeOfSide / aspectRatio);
        camera.viewportWidth = sizeOfSide;
        camera.viewportHeight = sizeOfSide / aspectRatio;
        camera.position.set(new Vector3(sizeOfSide/2, (sizeOfSide / aspectRatio) / 2, 0));

        background.setBounds(0, 0, sizeOfSide, sizeOfSide / aspectRatio );
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void comeToSettingsScreen(){
        game.setScreen(new SettingsScreen(game, this));
    }

    public void comeToStartScreen(){
        game.setScreen(StartScreen.getInstance(game));
    }

    public void comeBackToGame(){
        game.setScreen(gameScreen);
    }

    public void initializationButtons(){
        textureButtonSettings = new Texture("buttons/settings_button.png");
        textureButtonExitToMenu = new Texture("buttons/menu_button.png");
        textureButtonBack = new Texture("buttons/back_button.png");

        buttonSettings = new Button(sizeOfSide/2 - 4, 11 , 8, 3);
        buttonExitToMenu = new Button(sizeOfSide/2 - 4, 7 , 8, 3);
        buttonBack = new Button(sizeOfSide/2 - 4, 3 , 8, 3);

        buttonSettings.setSprite(textureButtonSettings);
        buttonExitToMenu.setSprite(textureButtonExitToMenu);
        buttonBack.setSprite(textureButtonBack);

        stage.addActor(background);
        stage.addActor(buttonSettings);
        stage.addActor(buttonExitToMenu);
        stage.addActor(buttonBack);

        buttonSettings.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeToSettingsScreen();
            }
        });

        buttonExitToMenu.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeToStartScreen();
            }
        });

        buttonBack.setClickHelper(new ClickHelper() {
            @Override
            public void clicked() {
                comeBackToGame();
            }
        });

        Gdx.input.setInputProcessor(stage);
    }
}
